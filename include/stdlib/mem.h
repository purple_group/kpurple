#ifndef STDLIB
#define STDLIB

void *memset(void *dst, int c, unsigned long long n);
void *bzero(void *dst, unsigned long long n);

#endif
